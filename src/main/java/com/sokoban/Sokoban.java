package com.sokoban;

import com.sokoban.components.GameWindow;
import com.sokoban.components.menu.panels.WelcomePanel;

import javax.swing.*;
import java.awt.*;

public class Sokoban extends JFrame {

    private Container container;
    private CardLayout cardLayout;

    public Sokoban() {
        container = getContentPane();
        cardLayout = new CardLayout();
        container.setLayout(cardLayout);

        GameWindow gameWindow = new GameWindow();

        container.add(new WelcomePanel(container, cardLayout, gameWindow));
        container.add(gameWindow);

        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            new Sokoban();
        });
    }

}
