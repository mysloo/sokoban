package com.sokoban.components;

import com.sokoban.components.menu.panels.MenuPanel;
import com.sokoban.elements.Drawable;
import com.sokoban.game.Game;
import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GameWindow extends JPanel implements Drawable {

    private final String imgFile = "background.jpg";

    private Image backgroundImg;
    private MenuPanel menuPanel;
    private Game game;
    private List<Game.Memento> savedStates = new ArrayList<>();

    public GameWindow() {
        backgroundImg = ImageUtils.createImage(imgFile);
        setLayout(new BorderLayout());
        requestFocusInWindow();
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        draw(graphics);
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.drawImage(backgroundImg, 0, 0, null);
    }

    public void initGame(Game game) {
        if (menuPanel == null) {
            menuPanel = new MenuPanel(this, BoxLayout.PAGE_AXIS);
            add(menuPanel, BorderLayout.EAST, 0);
        }
        setGame(game);
    }

    public void clearStates() {
        savedStates.clear();
    }

    public void removeStatesFromSavedStates(int number) {
        final int size = savedStates.size();
        for (int i = size - 1; i > number; i--) {
            savedStates.remove(i);
        }
    }

    public void adjustGame() {
        int toRemove = game.getSteps();
        removeStatesFromSavedStates(toRemove);
    }

    public void setGame(Game game) {
        if (this.game != null) {
            remove(this.game); //removing previous com.sokoban.game from this container
            revalidate();
        }
        this.game = game;
        add(game, BorderLayout.CENTER, 1);
    }

    public void saveToSavedStates(Game.Memento game) {
        savedStates.add(game);
    }

    public Game.Memento getStateFromSavedStates(int index) {
        return savedStates.get(index);
    }

    public Game getGame() {
        return game;
    }

}
