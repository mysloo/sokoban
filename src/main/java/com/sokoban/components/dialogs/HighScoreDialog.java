package com.sokoban.components.dialogs;

import com.sokoban.components.menu.buttons.BackButton;
import com.sokoban.components.menu.buttons.MenuButton;
import com.sokoban.components.menu.labels.MenuLabel;
import com.sokoban.components.menu.labels.RankingLabel;
import com.sokoban.components.menu.panels.CustomPanel;
import com.sokoban.elements.Drawable;
import com.sokoban.game.Score;
import com.sokoban.utils.DateUtils;
import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;
import java.util.TreeMap;

public class HighScoreDialog extends JDialog {
    private static final String IMAGE = "highscore_stats.png";
    private Image backgroundImage;

    public HighScoreDialog(boolean b, TreeMap<Score, String> stats) {
        super((Frame)null, null, b);
        this.backgroundImage = ImageUtils.createImage(IMAGE);
        setSize(new Dimension(backgroundImage.getWidth(null), backgroundImage.getHeight(null)));
        add(new HighScoreTable(stats, BoxLayout.Y_AXIS));
        setLocationRelativeTo(null);
        //!!
        setUndecorated(true);
        setResizable(false);
        setVisible(true);
    }

    private class HighScoreTable extends CustomPanel implements Drawable {
        private static final String DATE_FORMAT = "mm:ss";
        private final int MAX_ROWS = 4;

        public HighScoreTable(TreeMap<Score, String> stats, int layout) {
            super(layout);
            add(createRankingPanel(stats));
        }

        private JPanel createRankingPanel(TreeMap<Score, String> stats) {
            JPanel panel = new CustomPanel(BoxLayout.Y_AXIS);
            panel.add(Box.createRigidArea(new Dimension(0,40)));
            JPanel titlePanel = new CustomPanel();
            RankingLabel rankingLabel = new RankingLabel("Rank");
            RankingLabel playerLabel = new RankingLabel("Player");
            playerLabel.setMaximumSize(new Dimension(200, 50));
            RankingLabel scoreLabel = new RankingLabel("Time");
            RankingLabel stepsLabel = new RankingLabel("Steps");
            titlePanel.add(rankingLabel);
            titlePanel.add(Box.createRigidArea(new Dimension(5,0)));
            titlePanel.add(playerLabel);
            titlePanel.add(Box.createRigidArea(new Dimension(5,0)));
            titlePanel.add(scoreLabel);
            titlePanel.add(Box.createRigidArea(new Dimension(5,0)));
            titlePanel.add(stepsLabel);
            panel.add(titlePanel);

            displayRanking(stats, panel);

            JPanel exitPanel = new CustomPanel();
            MenuButton backButton = new BackButton("Back", (event) -> {
                dispose();
            });
            exitPanel.add(backButton);
            exitPanel.add(Box.createRigidArea(new Dimension(680, 0)));
            panel.add(exitPanel);
            return panel;
        }

        private void displayRanking(TreeMap<Score, String> stats, JPanel panel) {
            final int[] counter = {0};
            if (stats != null) {
                stats.forEach((k, v) -> {
                    if (counter[0]++ < MAX_ROWS) {
                        JPanel specPanel = new CustomPanel();
                        RankingLabel rank = new RankingLabel(counter[0] + ".");
                        RankingLabel player = new RankingLabel(v);
                        player.setMaximumSize(new Dimension(200, 50));
                        RankingLabel score = new RankingLabel(DateUtils.numberToStringDate(k.getTime(), DATE_FORMAT));
                        RankingLabel steps = new RankingLabel(String.valueOf(k.getSteps()));
                        specPanel.add(rank);
                        specPanel.add(Box.createRigidArea(new Dimension(5, 0)));
                        specPanel.add(player);
                        specPanel.add(Box.createRigidArea(new Dimension(5, 0)));
                        specPanel.add(score);
                        specPanel.add(Box.createRigidArea(new Dimension(5, 0)));
                        specPanel.add(steps);
                        panel.add(Box.createRigidArea(new Dimension(0, 5)));
                        panel.add(specPanel);
                    }
                });
                panel.add(Box.createRigidArea(new Dimension(0, 50)));
            } else {
                JPanel specPanel = new CustomPanel();
                MenuLabel info = new MenuLabel("  No one passed this level...");
                info.setForeground(Color.RED);
                info.setMaximumSize(new Dimension(350, 50));
                specPanel.add(info);
                panel.add(Box.createRigidArea(new Dimension(0, 100)));
                panel.add(specPanel);
                panel.add(Box.createRigidArea(new Dimension(0, 120)));
            }
        }

        @Override
        public void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            draw(graphics);
        }

        @Override
        public void draw(Graphics graphics) {
            graphics.drawImage(backgroundImage,0, 0, null);
        }
    }
}
