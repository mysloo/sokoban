package com.sokoban.components.dialogs.recap;

import com.sokoban.components.menu.buttons.MenuButton;
import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class RecapButton extends MenuButton {

    public RecapButton(String s, ActionListener listener) {
        super(s, listener);
        setIcon(new ImageIcon(ImageUtils.createImage("next_button.png")));
        setHorizontalTextPosition(SwingConstants.CENTER);
        setBorder(new EmptyBorder(0, 10, 0 ,10));
    }
}
