package com.sokoban.components.dialogs.recap;

import com.sokoban.components.GameWindow;
import com.sokoban.components.menu.buttons.MenuButton;
import com.sokoban.components.menu.buttons.SaveAndNextMap;
import com.sokoban.components.menu.labels.MenuLabel;
import com.sokoban.components.menu.panels.CustomPanel;
import com.sokoban.elements.Drawable;
import com.sokoban.game.MapLevel;
import com.sokoban.game.Score;
import com.sokoban.utils.DateUtils;
import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;

public class RecapDialog extends JDialog {
    private GameWindow gameWindow;
    private Image youWinImage;
    private Image backgroundImage;
    private Score score;

    public RecapDialog(boolean b, GameWindow gameWindow, Score score) {
        super((Frame)null, null, b);
        this.score = score;
        this.gameWindow = gameWindow;
        this.youWinImage = ImageUtils.createImage("you_win.jpg");
        this.backgroundImage = ImageUtils.createImage("endgame_background.png");
        setSize(new Dimension(youWinImage.getWidth(null), youWinImage.getHeight(null) + 200));
        add(new EndGamePanel(this, BoxLayout.Y_AXIS));

        setLocationRelativeTo(null);
        setUndecorated(true);
        setResizable(false);
        setVisible(true);
    }

    private class EndGamePanel extends CustomPanel implements Drawable {
        private static final String DATE_FORMAT = "mm:ss";

        public EndGamePanel(JDialog dialog, int layout) {
            super(layout);
            MenuLabel infoLabel = new RecapLabel("<html>Time: "
                    + DateUtils.numberToStringDate(score.getTime(), DATE_FORMAT)
                    + "<br/>Steps: "
                    + score.getSteps() + "</html>");
            JPanel panel = new CustomPanel();
            JTextField enterNameField = new RecapTextField("Enter your name");
            MenuButton button = new RecapButton("Next", new SaveAndNextMap(gameWindow, dialog, enterNameField, score));
            add(Box.createRigidArea(new Dimension(0, youWinImage.getHeight(null) + 20)));
            add(infoLabel);
            add(Box.createRigidArea(new Dimension(0,20)));
            panel.add(enterNameField);
            panel.add(Box.createRigidArea(new Dimension(20,0)));
            panel.add(button);
            add(panel);
        }

        @Override
        public void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            draw(graphics);
        }

        @Override
        public void draw(Graphics graphics) {
            graphics.drawImage(youWinImage, 0, 0, null);
            graphics.drawImage(backgroundImage,0, youWinImage.getHeight(null), null);
        }

    }

    public static void main(String[] args) {
        new RecapDialog(true, null, new Score(8015, 30, MapLevel.ONE));
    }

}
