package com.sokoban.components.dialogs.recap;

import com.sokoban.components.menu.labels.MenuLabel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RecapLabel extends MenuLabel {

    public RecapLabel(String s) {
        super(s);
        setMaximumSize(new Dimension(325, 70));
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GREEN),
                new EmptyBorder(10, 10, 10 ,10)));
        setAlignmentX(Component.CENTER_ALIGNMENT);
        setHorizontalAlignment(SwingConstants.CENTER);
    }
}
