package com.sokoban.components.dialogs.recap;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RecapTextField extends JTextField {
    private static final Font font = new Font(Font.MONOSPACED, Font.PLAIN, 18);

    public RecapTextField(String s) {
        super(s);
        setOpaque(false);
        setMaximumSize(new Dimension(200, 50));
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                new EmptyBorder(0, 10, 0 ,10)));
        setForeground(Color.LIGHT_GRAY);
        setFont(font);
    }
}
