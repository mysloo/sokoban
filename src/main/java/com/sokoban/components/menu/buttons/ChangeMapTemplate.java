package com.sokoban.components.menu.buttons;

import com.sokoban.components.GameWindow;
import com.sokoban.game.Game;
import com.sokoban.game.MapLevel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class ChangeMapTemplate implements ActionListener {
    protected GameWindow gameWindow;

    public ChangeMapTemplate(GameWindow gameWindow) {
        this.gameWindow = gameWindow;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Game game = gameWindow.getGame();
        MapLevel level = game.getMap().getLevel();
        MapLevel newLevel = setSubsequentLevel(level);
        Game newGame = new Game(newLevel, gameWindow);
        game.getGameController().update(newGame.getMap());
        gameWindow.setGame(newGame);

    }

    public abstract MapLevel setSubsequentLevel(MapLevel level);

}
