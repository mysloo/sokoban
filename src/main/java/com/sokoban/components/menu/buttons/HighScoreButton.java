package com.sokoban.components.menu.buttons;

import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class HighScoreButton extends MenuButton {
    public HighScoreButton(String s, ActionListener listener) {
        super(s, listener);
        setIcon(new ImageIcon(ImageUtils.createImage("highscore_button.png")));
        setHorizontalTextPosition(SwingConstants.CENTER);
        setBorder(new EmptyBorder(0, 0, 0 ,0));
    }
}
