package com.sokoban.components.menu.buttons;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MenuButton extends JButton {
    private static Font font = new Font(Font.MONOSPACED, Font.PLAIN, 18);

    public MenuButton(String s, ActionListener listener) {
        super(s);
        setFocusable(false);
        setOpaque(false);
        setContentAreaFilled(false);
        setPreferredSize(new Dimension(30, 30));
        setFont(font);
        setForeground(Color.WHITE);
        addActionListener(listener);
    }
}
