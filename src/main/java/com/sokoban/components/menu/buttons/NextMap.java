package com.sokoban.components.menu.buttons;

import com.sokoban.components.GameWindow;
import com.sokoban.game.MapLevel;

public class NextMap extends ChangeMapTemplate {

    public NextMap(GameWindow gameWindow) {
        super(gameWindow);
    }

    @Override
    public MapLevel setSubsequentLevel(MapLevel level) {
        return level.setUpNextLevel(level);
    }
}
