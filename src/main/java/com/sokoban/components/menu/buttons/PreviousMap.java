package com.sokoban.components.menu.buttons;

import com.sokoban.components.GameWindow;
import com.sokoban.game.MapLevel;

public class PreviousMap extends ChangeMapTemplate {

    public PreviousMap(GameWindow gameWindow) {
        super(gameWindow);
    }

    @Override
    public MapLevel setSubsequentLevel(MapLevel level) {
        return level.getPreviousLevel(level);
    }
}
