package com.sokoban.components.menu.buttons;

import com.sokoban.components.GameWindow;
import com.sokoban.game.MapLevel;
import com.sokoban.game.Score;
import com.sokoban.utils.SerializationUtils;

import javax.swing.*;
import java.io.IOException;
import java.util.TreeMap;

public class SaveAndNextMap extends ChangeMapTemplate {
    private JTextField enterNameTextField;
    private Score score;
    private JDialog dialog;

    public SaveAndNextMap(GameWindow gameWindow, JDialog dialog, JTextField enterNameTextField, Score score) {
        super(gameWindow);
        this.dialog = dialog;
        this.enterNameTextField = enterNameTextField;
        this.score = score;
    }

    @Override
    public MapLevel setSubsequentLevel(MapLevel level) {
        saveToRanking(enterNameTextField.getText());
        dialog.dispose();
        return level.setUpNextLevel(level);
    }

    private void saveToRanking(String name) {
        MapLevel level = score.getLevel();
        try {
            Object o = SerializationUtils.deserialize("highscore_map" + level.getIndex() +  ".txt");;
            if (o != null) {
                TreeMap<Score, String> rankingMap = (TreeMap) o;
                rankingMap.put(score, name);
                SerializationUtils.serialize(rankingMap, "highscore_map" + level.getIndex() + ".txt");
            }
        } catch (IOException | ClassNotFoundException e) {
            TreeMap<Score, String> rankingMap = new TreeMap<>();
            rankingMap.put(score, name);
            try {
                SerializationUtils.serialize(rankingMap, "highscore_map" + level.getIndex() +  ".txt");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
