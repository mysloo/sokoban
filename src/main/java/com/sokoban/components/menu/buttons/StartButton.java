package com.sokoban.components.menu.buttons;

import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class StartButton extends MenuButton {

    public StartButton(String s, ActionListener listener) {
        super(s, listener);
        setIcon(new ImageIcon(ImageUtils.createImage("start.png")));
        setHorizontalTextPosition(SwingConstants.CENTER);
        setBorder(new EmptyBorder(0, 0, 0 ,0));
    }
}
