package com.sokoban.components.menu.buttons;

import com.sokoban.components.GameWindow;
import com.sokoban.game.Game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StepStrategy implements ActionListener {

    private final GameWindow gameWindow;
    private int steps;

    public StepStrategy(GameWindow gameWindow, int steps) {
        this.gameWindow = gameWindow;
        this.steps = steps;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        int undoCounter = gameWindow.getGame().getSteps() + steps + 1;
        try {
            Game.Memento memento = gameWindow.getStateFromSavedStates(undoCounter);
            gameWindow.getGame().restoreFromMemento(memento);
            gameWindow.setGame(gameWindow.getGame().getState());
        } catch (IndexOutOfBoundsException e) {
            //e.printStackTrace();
        }
    }
}
