package com.sokoban.components.menu.labels;

import com.sokoban.components.GameWindow;
import com.sokoban.elements.GameObserver;
import com.sokoban.game.Game;
import com.sokoban.game.GameController;

public class MapLevelLabel extends MenuLabel implements GameObserver {

    public MapLevelLabel(String s, GameWindow gameWindow) {
        super(s);
        GameController gameController = gameWindow.getGame().getGameController();
        gameController.addObserver(this);
    }

    @Override
    public void update(Game game) {
        int level = game.getMap().getLevel().getIndex();
        String text = String.valueOf(level);
        if (level < 10) {
            text = " " + level + " ";
        } else if (level < 100) {
            text = " " + level;
        }
        this.setText(text);
    }
}
