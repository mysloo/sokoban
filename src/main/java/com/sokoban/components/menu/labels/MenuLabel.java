package com.sokoban.components.menu.labels;

import javax.swing.*;
import java.awt.*;

public class MenuLabel extends JLabel {
    private static Font font = new Font(Font.MONOSPACED, Font.PLAIN, 18);

    public MenuLabel(String s) {
        super(s);
        setFont(font);
        setForeground(Color.WHITE);
        setPreferredSize(new Dimension(50, 50));
    }
}
