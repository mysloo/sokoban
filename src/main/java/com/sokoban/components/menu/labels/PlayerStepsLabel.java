package com.sokoban.components.menu.labels;

import com.sokoban.components.GameWindow;
import com.sokoban.elements.GameObserver;
import com.sokoban.game.Game;
import com.sokoban.game.GameController;

public class PlayerStepsLabel extends MenuLabel implements GameObserver {

    public PlayerStepsLabel(String s, GameWindow gameWindow) {
        super(s);
        GameController gameController = gameWindow.getGame().getGameController();
        gameController.addObserver(this);
    }

    @Override
    public void update(Game game) {
        String text = String.valueOf(game.getSteps());
        int steps = game.getSteps();
        if (steps < 10) {
            text = " " + steps + " ";
        } else if (steps < 100) {
            text = " " + steps;
        }
        this.setText(text);
    }
}
