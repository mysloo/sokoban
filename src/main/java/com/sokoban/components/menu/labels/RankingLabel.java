package com.sokoban.components.menu.labels;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class RankingLabel extends MenuLabel {
    private static Font font = new Font(Font.MONOSPACED, Font.BOLD, 15);

    public RankingLabel(String s) {
        super(s);
        setFont(font);
        setMaximumSize(new Dimension(100,50));
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                new EmptyBorder(10, 20, 10, 20)));
    }
}
