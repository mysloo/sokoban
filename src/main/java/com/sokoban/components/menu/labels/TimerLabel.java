package com.sokoban.components.menu.labels;

import com.sokoban.components.GameWindow;
import com.sokoban.elements.GameObserver;
import com.sokoban.game.Game;
import com.sokoban.game.GameController;
import com.sokoban.utils.DateUtils;

public class TimerLabel extends MenuLabel implements GameObserver {
    private static final String START_TIME = "00:00";
    private static final String DATE_FORMAT = "mm:ss";

    public TimerLabel(GameWindow gameWindow) {
        super(START_TIME);
        GameController gameController = gameWindow.getGame().getGameController();
        gameController.addObserver(this);
    }

    @Override
    public void update(Game game) {
        long time = game.getGameController().getTimer();
        String text = DateUtils.numberToStringDate(time, DATE_FORMAT);
        this.setText(text);
    }

}
