package com.sokoban.components.menu.panels;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CompoundPanel extends CustomPanel {

    public CompoundPanel(JButton prev, JLabel label, JButton next) {
        Dimension dim = new Dimension(15, 0);
        add(prev);
        add(Box.createRigidArea(dim));
        add(label);
        add(Box.createRigidArea(dim));
        add(next);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY),
                new EmptyBorder(5, 10, 5, 10)));
    }
}
