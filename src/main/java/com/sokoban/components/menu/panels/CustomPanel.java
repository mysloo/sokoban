package com.sokoban.components.menu.panels;

import javax.swing.*;

public class CustomPanel extends JPanel {

    public CustomPanel() {
        setVisible(true);
        setOpaque(false);
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
    }

    public CustomPanel(int layoutType) {
        setVisible(true);
        setOpaque(false);
        setLayout(new BoxLayout(this, layoutType));
    }
}
