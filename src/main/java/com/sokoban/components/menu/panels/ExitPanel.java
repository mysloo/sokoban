package com.sokoban.components.menu.panels;

import com.sokoban.components.menu.buttons.ExitButton;

import javax.swing.*;
import java.awt.*;

public class ExitPanel extends CustomPanel {

    public ExitPanel() {
        ExitButton exitButton = new ExitButton("", (event) -> System.exit(0));
        add(Box.createRigidArea(new Dimension(190, 0)));
        add(exitButton);
    }
}
