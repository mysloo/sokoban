package com.sokoban.components.menu.panels;

import com.sokoban.components.GameWindow;
import com.sokoban.components.dialogs.HighScoreDialog;
import com.sokoban.components.menu.buttons.HighScoreButton;
import com.sokoban.components.menu.buttons.MenuButton;
import com.sokoban.elements.GameObserver;
import com.sokoban.game.Game;
import com.sokoban.game.GameController;
import com.sokoban.game.MapLevel;
import com.sokoban.game.Score;
import com.sokoban.utils.SerializationUtils;

import javax.swing.*;
import java.io.IOException;
import java.util.TreeMap;

public class HighScoresPanel extends CustomPanel implements GameObserver {

    private MapLevel level;

    public HighScoresPanel(GameWindow gameWindow) {
        GameController gameController = gameWindow.getGame().getGameController();
        gameController.addObserver(this);
        add(createHighScoreButton());
    }

    private JPanel createHighScoreButton() {
        JPanel panel = new CustomPanel();
        MenuButton highScoreButton = new HighScoreButton("HIGH SCORES", (event) -> {
            TreeMap<Score, String> rankingMap = null;
            try {
                Object o = SerializationUtils.deserialize("highscore_map" + level.getIndex() + ".txt");
                rankingMap = (TreeMap) o;
            } catch (IOException | ClassNotFoundException e) {
             //   e.printStackTrace();
            }
            new HighScoreDialog(true, rankingMap);
        });
        panel.add(highScoreButton);
        return panel;
    }



    @Override
    public void update(Game game) {
        level = game.getMap().getLevel();
    }
}
