package com.sokoban.components.menu.panels;

import com.sokoban.components.GameWindow;
import com.sokoban.components.menu.buttons.MenuButton;
import com.sokoban.components.menu.buttons.NextMap;
import com.sokoban.components.menu.buttons.PreviousMap;
import com.sokoban.components.menu.buttons.StepStrategy;
import com.sokoban.components.menu.labels.MapLevelLabel;
import com.sokoban.components.menu.labels.PlayerStepsLabel;
import com.sokoban.components.menu.labels.TimerLabel;
import com.sokoban.elements.Drawable;
import com.sokoban.utils.ImageUtils;

import javax.swing.*;
import java.awt.*;

public class MenuPanel extends CustomPanel implements Drawable {
    private final GameWindow gameWindow;
    private Image menuImg;


    public MenuPanel(GameWindow gameWindow, int boxLayoutType) {
        super(boxLayoutType);
        this.gameWindow = gameWindow;
        menuImg = ImageUtils.createImage("menu.png");
        setPreferredSize(new Dimension(menuImg.getWidth(null), menuImg.getHeight(null) + 300));
        add(Box.createRigidArea(new Dimension(0,10)));
        add(new ExitPanel());
        add(Box.createRigidArea(new Dimension(0,100)));
        add(new HighScoresPanel(gameWindow));
        add(Box.createRigidArea(new Dimension(0,30)));
        add(new TitlePanel("Map"));
        add(Box.createRigidArea(new Dimension(0,10)));
        add(createMapPanel("<<", " ", ">>"));
        add(Box.createRigidArea(new Dimension(0,30)));
        add(new TitlePanel("Steps"));
        add(Box.createRigidArea(new Dimension(0,10)));
        add(createStepsPanel("<<", " ", ">>"));
        add(Box.createRigidArea(new Dimension(0,30)));
        add(new TitlePanel("Timer"));
        add(Box.createRigidArea(new Dimension(0,10)));
        TimerLabel timerLabel = new TimerLabel(gameWindow);
        add(new TimerPanel(timerLabel));
    }

    private JPanel createStepsPanel(String text1, String text2, String text3) {
        JButton prev = new MenuButton(text1, new StepStrategy(gameWindow, -1));
        JLabel playerStepsLabel = new PlayerStepsLabel(text2, gameWindow);
        JButton next = new MenuButton(text3, new StepStrategy(gameWindow, 1));
        JPanel panel = new CompoundPanel(prev, playerStepsLabel, next);
        return panel;
    }

    private JPanel createMapPanel(String text1, String text2, String text3) {
        JButton prev = new MenuButton(text1, new PreviousMap(gameWindow));
        JLabel mapLevelLabel = new MapLevelLabel(text2, gameWindow);
        JButton next = new MenuButton(text3, new NextMap(gameWindow));
        JPanel panel = new CompoundPanel(prev, mapLevelLabel, next);
        return panel;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        draw(graphics);
    }


    @Override
    public void draw(Graphics graphics) {
        graphics.drawImage(menuImg, 0, 70, null);
    }

}
