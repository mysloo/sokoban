package com.sokoban.components.menu.panels;

import com.sokoban.components.menu.labels.TimerLabel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class TimerPanel extends CustomPanel {

    public TimerPanel(TimerLabel timerLabel) {
        add(timerLabel);
        setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.RED),
                new EmptyBorder(15, 70, 15, 70)));
    }
}
