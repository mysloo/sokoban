package com.sokoban.components.menu.panels;

import com.sokoban.components.menu.labels.MenuLabel;

import javax.swing.*;

public class TitlePanel extends CustomPanel {

    public TitlePanel(String text) {
        JLabel stepsLabel = new MenuLabel(text);
        add(stepsLabel);
    }
}
