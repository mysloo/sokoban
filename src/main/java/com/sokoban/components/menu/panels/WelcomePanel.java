package com.sokoban.components.menu.panels;

import com.sokoban.components.GameWindow;
import com.sokoban.components.menu.buttons.MenuButton;
import com.sokoban.components.menu.buttons.StartButton;
import com.sokoban.elements.Drawable;
import com.sokoban.game.Game;
import com.sokoban.game.MapLevel;
import com.sokoban.utils.ImageUtils;
import javafx.scene.media.AudioClip;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Paths;

public class WelcomePanel extends CustomPanel implements Drawable {
    private final String PROP_IMAGE_NAME = "sokoban.png";
    private static final String SOUND_PATH = "src/main/resources/sound/main_sound.wav";
    private final Image image;
    private final Container container;
    private final CardLayout layout;
    private GameWindow gameWindow;

    public WelcomePanel(Container container, CardLayout layout, GameWindow gameWindow) {
        super(BoxLayout.Y_AXIS);
        this.container = container;
        this.layout = layout;
        this.gameWindow = gameWindow;
        this.image = ImageUtils.createImage(PROP_IMAGE_NAME);
        setPreferredSize(new Dimension(image.getWidth(null), image.getHeight(null)));
        add(Box.createRigidArea(new Dimension(0,620)));
        add(createStartButtonPanel());

        AudioClip plonkSound = new AudioClip(Paths.get(SOUND_PATH).toUri().toString());
        plonkSound.setCycleCount(Integer.MAX_VALUE);
        plonkSound.play();
    }

    private JPanel createStartButtonPanel() {
        JPanel panel = new CustomPanel();
        MenuButton startButton = new StartButton("", (event) -> {
            layout.next(container);
            new Game(MapLevel.ONE, gameWindow);
        });
        panel.add(startButton);
        return panel;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        draw(graphics);
    }


    @Override
    public void draw(Graphics graphics) {
        graphics.drawImage(image, 0, 0, null);
    }

}
