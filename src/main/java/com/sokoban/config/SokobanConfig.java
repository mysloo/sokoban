package com.sokoban.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SokobanConfig {
    private static SokobanConfig ourInstance = new SokobanConfig();
    private static final String configPath = "src/main/resources/sokoban.properties";

    private final String SOKOBAN = "sokoban.";
    private final String IMAGES = "images.";
    private final String WINDOW = "window.";
    private final String MAPS = "maps.";
    private final String PATH = "path";
    private final String EXTENSION ="extension";
    private final String MAP = "map.";
    private final String TILE ="tile.";
    private final String DEFAULT = "default.";
    private final String WIDTH = "width.";
    private final String HEIGHT = "height.";
    private final String SCALABILITY = "scalability";
    private final String SIZE = "size";
    private final String ELEMENT = "element.";
    private final String UNREACHED_FIELD = "unreached_field.";
    private final String WALL = "wall.";
    private final String FLOOR = "floor.";
    private final String ENDPOINT = "endpoint.";
    private final String LEVER = "lever.";
    private final String OBSTACLE = "obstacle.";
    private final String BOX = "box.";
    private final String PLAYER = "player.";
    private final String VALUE = "value";

    private Properties props = new Properties();

    private SokobanConfig() {
        try {
            props.load(new FileInputStream(configPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getImagesPath() {
        return props.getProperty(SOKOBAN + IMAGES + PATH);
    }

    public String getMapsPath() {
        return props.getProperty(SOKOBAN + MAPS + PATH);
    }

    public String getProperExtension() { return props.getProperty(SOKOBAN + MAPS + EXTENSION); }

    public int getTileSize() {
        return Integer.parseInt(props.getProperty(SOKOBAN + MAP + TILE + SIZE));
    }

    public int getDefaultTileSize() {
        return Integer.parseInt(props.getProperty(SOKOBAN + MAP + DEFAULT + TILE + SIZE));
    }

    public int getMapWidth() {
        return Integer.parseInt(props.getProperty(SOKOBAN + MAP + WIDTH + SIZE));
    }

    public int getMapHeight() {
        return Integer.parseInt(props.getProperty(SOKOBAN + MAP + HEIGHT + SIZE));
    }

    public int getScalability() {
        return Integer.parseInt(props.getProperty(SOKOBAN + MAP + SCALABILITY));
    }

    public int getUnreachedFieldValue() {
        return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + UNREACHED_FIELD + VALUE));
    }

    public int getWallValue() {
        return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + WALL + VALUE));
    }

    public int getFloorValue() {
        return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + FLOOR + VALUE));
    }

    public int getEndpointValue() {
        return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + ENDPOINT + VALUE));
    }

    public int getLeverValue() { return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + LEVER + VALUE)); }

    public int getObstacleValue() { return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + OBSTACLE + VALUE)); }

    public int getBoxValue() {
        return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + BOX + VALUE));
    }

    public int getPlayerValue() {
        return Integer.parseInt(props.getProperty(SOKOBAN + ELEMENT + PLAYER + VALUE));
    }


    public static SokobanConfig getInstance() {
        return ourInstance;
    }
}
