package com.sokoban.elements;

import java.awt.*;

public class Box extends Element implements Pushable {
    private static final String file = "Pot.gif";

    public Box(ElementStrategy strategy) {
        super(file, strategy);
    }

    @Override
    public Point push(Direction direction, Point point) {
        Point newPoint = new Point(point);
        newPoint.x += direction.getX();
        newPoint.y += direction.getY();
        return newPoint;
    }

    @Override
    public int getValue() {
        return config.getBoxValue();
    }
}
