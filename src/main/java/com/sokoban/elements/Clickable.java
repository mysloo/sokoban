package com.sokoban.elements;

public interface Clickable {
    void setAllowed(boolean allowed);
}
