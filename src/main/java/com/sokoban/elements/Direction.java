package com.sokoban.elements;

public enum Direction {
    STANDING(0,0, -1),
    LEFT(   -1, 0, 4),
    RIGHT(   1, 0, 12),
    UP(      0, -1, 8),
    DOWN(    0, 1, 0);

    private int x;
    private int y;
    private int firstFrame;

    Direction(int x, int y, int first) {
        this.x = x;
        this.y = y;
        this.firstFrame = first;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getFirstFrame() {
        return firstFrame;
    }
}
