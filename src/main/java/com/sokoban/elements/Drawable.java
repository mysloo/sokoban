package com.sokoban.elements;

import java.awt.*;

public interface Drawable {
     default void draw(Graphics graphics) { }
     default void draw(Graphics graphics, Point point) { }
}
