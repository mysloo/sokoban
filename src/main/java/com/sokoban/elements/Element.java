package com.sokoban.elements;

import com.sokoban.config.SokobanConfig;
import com.sokoban.game.Game;
import com.sokoban.utils.ImageUtils;

import java.awt.*;

public abstract class Element implements Drawable, Comparable<Element> {
    protected static final SokobanConfig config = SokobanConfig.getInstance();
    protected static final int DEFAULT_SIZE = config.getDefaultTileSize();
    protected static final int TILE_SIZE = config.getTileSize();
    protected static final int SCALE = config.getScalability();
    protected Image img;
    protected int height;
    protected int width;
    protected final int value;
    protected ElementStrategy strategy;

    public Element(String imgFile, ElementStrategy strategy) {
        img =  ImageUtils.createImage(imgFile);
        height = img.getHeight(null);
        width = img.getWidth(null);
        this.value = getValue();
        this.strategy = strategy;
    }

    public abstract int getValue();

    @Override
    public void draw(Graphics g, Point p) {
        g.drawImage(img, p.x * TILE_SIZE, p.y * TILE_SIZE,width * SCALE, height * SCALE, null);
    }

    @Override
    public int compareTo(Element that) {
        return this.getValue() - that.getValue();
    }

    public boolean doAction(Game game) {
        return strategy.performAction(game);
    }
}
