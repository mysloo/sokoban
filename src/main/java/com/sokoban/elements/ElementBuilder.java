package com.sokoban.elements;

import com.sokoban.game.GameMap;
import com.sokoban.game.MapLevel;

import java.awt.*;
import java.util.LinkedList;

public class ElementBuilder {
    private GameMap map;
    private int offsetX;
    private int offsetY;

    public ElementBuilder(MapLevel level) {
        map = new GameMap(level);
        map.put(ElementFactory.getElementType(Wall.class),
                new LinkedList<>());
        map.put(ElementFactory.getElementType(Box.class),
                new LinkedList<>());
        map.put(ElementFactory.getElementType(Player.class),
                new LinkedList<>());
        map.put(ElementFactory.getElementType(Endpoint.class),
                new LinkedList<>());
        map.put(ElementFactory.getElementType(Floor.class),
                new LinkedList<>());
        map.put(ElementFactory.getElementType(UnreachedField.class),
                new LinkedList<>());
        map.put(ElementFactory.getElementType(Lever.class), new LinkedList<>());
        map.put(ElementFactory.getElementType(Obstacle.class), new LinkedList<>());

    }

    public void setOffsets(Point offsets) {
        offsetX = (int) offsets.getX();
        offsetY = (int) offsets.getY();
    }

    public void createWall(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        map.get(ElementFactory.getElementType(Wall.class)).add(p);
    }

    public void createBox(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        map.get(ElementFactory.getElementType(Box.class)).add(p);
        map.incrementBoxesAmount();
        createFloor(row, column);
    }

    public Player createPlayer(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        Player player = (Player)ElementFactory.getElementType(Player.class);
        map.get(player).add(p);
        createFloor(row, column);
        return player;
    }

    public void createEndpoint(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        map.get(ElementFactory.getElementType(Endpoint.class))
                .add(p);
        createFloor(row, column);
    }

    public void createFloor(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        map.get(ElementFactory.getElementType(Floor.class))
                .add(p);
    }

    public void createUnreachedField(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        map.get(ElementFactory.getElementType(UnreachedField.class))
                .add(p);
    }

    public Lever createLever(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        Lever lever = (Lever) ElementFactory.getElementType(Lever.class);
        map.get(lever).add(p);
        createFloor(row, column);
        return lever;
    }

    public Obstacle createObstacle(int row, int column) {
        Point p = new Point(offsetX + row, offsetY + column);
        Obstacle obstacle = (Obstacle) ElementFactory.getElementType(Obstacle.class);
        map.get(obstacle).add(p);
        createFloor(row, column);
        return obstacle;
    }

    public GameMap getMap() {
        return map;
    }
}
