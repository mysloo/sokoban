package com.sokoban.elements;

import com.sokoban.elements.strategies.DefaultElementStrategy;
import com.sokoban.elements.strategies.DragLeverElementStrategy;
import com.sokoban.elements.strategies.MoveElementStrategy;
import com.sokoban.elements.strategies.PushElementStrategy;

import java.util.HashMap;

public class ElementFactory {

    private static HashMap<Class, Element> elementTypes = new HashMap<>();

    public static Element getElementType(Class clazz) {
        Element result = elementTypes.get(clazz);
        if (result == null) {
            if (Player.class.equals(clazz)) {
                result = new Player(new MoveElementStrategy());
            } else if (Wall.class.equals(clazz)) {
                result = new Wall(new DefaultElementStrategy());
            } else if (Endpoint.class.equals(clazz)) {
                result = new Endpoint(new DefaultElementStrategy());
            } else if (Box.class.equals(clazz)) {
                result = new Box(new PushElementStrategy());
            } else if (Floor.class.equals(clazz)) {
                result = new Floor(new DefaultElementStrategy());
            } else if (UnreachedField.class.equals(clazz)) {
                result = new UnreachedField(new DefaultElementStrategy());
            } else if (Lever.class.equals(clazz)) {
                result = new Lever(new DragLeverElementStrategy());
            } else if (Obstacle.class.equals(clazz)) {
                result = new Obstacle(new DefaultElementStrategy());
            }
            elementTypes.put(clazz, result);
        }
        return result;
    }
}
