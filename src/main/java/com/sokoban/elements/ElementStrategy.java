package com.sokoban.elements;

import com.sokoban.game.Game;

public interface ElementStrategy {
    boolean performAction(Game game);
}
