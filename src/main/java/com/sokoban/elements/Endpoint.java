package com.sokoban.elements;

public class Endpoint extends Element implements Standable {
    private static final String file = "green_light.gif";
    private boolean standingFlag;

    public Endpoint(ElementStrategy strategy) {
        super(file, strategy);
        setStandingFlag(true);
    }

    @Override
    public int getValue() {
        return config.getEndpointValue();
    }

    @Override
    public void setStandingFlag(boolean isCapable) {
        standingFlag = isCapable;
    }

    @Override
    public boolean isStandable() {
        return standingFlag;
    }
}
