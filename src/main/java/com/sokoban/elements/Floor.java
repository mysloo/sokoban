package com.sokoban.elements;

public class Floor extends Element implements Standable {
    private static final String file = "black_marble_floor.gif";
    private boolean standableFlag;

    public Floor(ElementStrategy strategy) {
        super(file, strategy);
        setStandingFlag(true);
    }

    @Override
    public int getValue() {
        return config.getFloorValue();
    }

    @Override
    public void setStandingFlag(boolean isCapable) {
        standableFlag = isCapable;
    }

    @Override
    public boolean isStandable() {
        return standableFlag;
    }
}
