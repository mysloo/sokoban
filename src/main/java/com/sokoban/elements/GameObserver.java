package com.sokoban.elements;

import com.sokoban.game.Game;

public interface GameObserver {
    void update(Game game);
}
