package com.sokoban.elements;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Lever extends Element implements KeyListener, Clickable {
    private static final String imgFile = "lever.png";
    private int frameCounter;
    private boolean isAllowed;
    private Obstacle obstacle;

    public Lever(ElementStrategy strategy) {
        super(imgFile, strategy);
    }


    public void setObstacle(Obstacle obstacle) {
        this.obstacle = obstacle;
    }

    @Override
    public int getValue() {
        return config.getLeverValue();
    }

    @Override
    public void draw(Graphics graphics, Point p) {
        graphics.drawImage(img, p.x * TILE_SIZE, p.y * TILE_SIZE,
                p.x * TILE_SIZE + SCALE * DEFAULT_SIZE, p.y * TILE_SIZE + SCALE * DEFAULT_SIZE,
                height * frameCounter, 0,
                height * frameCounter + height, height,
                null);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (isAllowed) {
            if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                if (frameCounter == 0) {
                    frameCounter = 1;
                    obstacle.openObstacle();
                } else {
                    frameCounter = 0;
                    obstacle.closeObstacle();
                }
            }
            isAllowed = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void setAllowed(boolean allowed) {
        isAllowed = allowed;
    }

}
