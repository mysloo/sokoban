package com.sokoban.elements;

import java.awt.*;

public interface Movable {

    Point move(Point p);
}
