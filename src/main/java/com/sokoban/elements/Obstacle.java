package com.sokoban.elements;

import java.awt.*;

public class Obstacle extends Element implements Standable {
    private static final String file = "door.png";
    private boolean standableFlag;
    private int frameCounter = 1;

    public Obstacle(ElementStrategy strategy) {
        super(file, strategy);
    }

    @Override
    public int getValue() {
        return config.getObstacleValue();
    }

    public void openObstacle() {
        setStandingFlag(true);
        frameCounter = 0;
    }

    public void closeObstacle() {
        setStandingFlag(false);
        frameCounter = 1;
    }

    @Override
    public void draw(Graphics graphics, Point p) {
        graphics.drawImage(img, p.x * TILE_SIZE - DEFAULT_SIZE, p.y * TILE_SIZE - DEFAULT_SIZE,
                p.x * TILE_SIZE + SCALE * DEFAULT_SIZE, p.y * TILE_SIZE + SCALE * DEFAULT_SIZE,
                height * frameCounter, 0,
                height * frameCounter + height, height,
                null);
    }

    @Override
    public void setStandingFlag(boolean isCapable) {
        standableFlag = isCapable;
    }

    @Override
    public boolean isStandable() {
        return standableFlag;
    }
}
