package com.sokoban.elements;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Player extends Element implements KeyListener, Movable {
    private static final String file = "citizen.png";

    private Direction direction = Direction.STANDING;
    private Direction lastDirection = Direction.STANDING;
    private int frameCounter;
    private int lastFrame;
    private int numberOfFrames;

    public Player(ElementStrategy strategy) {
        super(file, strategy);
        this.numberOfFrames = width/height;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) { }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                left();
                break;
            case KeyEvent.VK_RIGHT:
                right();
                break;
            case KeyEvent.VK_UP:
                up();
                break;
            case KeyEvent.VK_DOWN:
                down();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if (keyEvent.getSource().equals(this)) {
            stopPlayer();
        }
    }

    public Direction getLastDirection() {
        return lastDirection;
    }


    public Direction getDirection() {
        return direction;
    }

    @Override
    public Point move(Point p) {
        Point point = new Point(p);
        point.x += direction.getX();
        point.y += direction.getY();
        this.frameCounter = direction.getFirstFrame();
        this.lastFrame = this.frameCounter + 3;
        stopPlayer();
        return point;
    }

    private void stopPlayer() {
        lastDirection = direction;
        direction = Direction.STANDING;
    }

    private void left() {
        direction = Direction.LEFT;
    }

    private void right() {
        direction = Direction.RIGHT;
    }

    private void up() {
        direction = Direction.UP;
    }

    private void down() {
        direction = Direction.DOWN;
    }


    public void draw(Graphics g, Point p) {
        g.drawImage(img, p.x * TILE_SIZE - DEFAULT_SIZE, p.y * TILE_SIZE - DEFAULT_SIZE,
                p.x * TILE_SIZE + SCALE * DEFAULT_SIZE, p.y * TILE_SIZE + SCALE * DEFAULT_SIZE,
                height * (frameCounter % numberOfFrames), 0,
                height * (frameCounter % numberOfFrames) + height, height,
                null);
    }

    public void tick() {
        if (frameCounter != lastFrame){
            frameCounter++;
        }
    }

    public boolean isMoved() {
        return direction != Direction.STANDING;
    }

    public Point push(Pushable element, Point p) {
        return element.push(direction, p);
    }

    @Override
    public int getValue() {
        return config.getPlayerValue();
    }

}
