package com.sokoban.elements;

import java.awt.*;

public interface Pushable {
    Point push(Direction direction, Point point);
}
