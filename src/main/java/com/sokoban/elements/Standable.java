package com.sokoban.elements;

public interface Standable {

    void setStandingFlag(boolean isCapable);

    boolean isStandable();
}
