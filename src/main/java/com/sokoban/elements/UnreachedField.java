package com.sokoban.elements;


public class UnreachedField extends Element {
    private static final String file = "Snow_Tile.gif";

    public UnreachedField(ElementStrategy strategy) {
        super(file, strategy);
    }


    @Override
    public int getValue() {
        return config.getUnreachedFieldValue();
    }
}