package com.sokoban.elements;

public class Wall extends Element {
    private static final String file = "ice_cube.gif";

    public Wall(ElementStrategy strategy) {
        super(file, strategy);
    }

    @Override
    public int getValue() {
        return config.getWallValue();
    }
}
