package com.sokoban.elements.strategies;

import com.sokoban.elements.ElementStrategy;
import com.sokoban.game.Game;

public class DefaultElementStrategy implements ElementStrategy {

    @Override
    public boolean performAction(Game game) {
        return false;
    }
}
