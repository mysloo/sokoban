package com.sokoban.elements.strategies;

import com.sokoban.elements.*;
import com.sokoban.game.Game;
import com.sokoban.game.GameMap;

import java.awt.*;

public class DragLeverElementStrategy implements ElementStrategy {

    @Override
    public boolean performAction(Game game) {
        GameMap map = game.getMap();
        Player player = game.getPlayer();
        Point playerPoint = map.getPlayerPoint();

        Point pointInFront = new Point(playerPoint.x + player.getLastDirection().getX(),
                playerPoint.y + player.getLastDirection().getY());
        Element inFront = map.getElement(pointInFront);
        if (inFront instanceof Clickable) {
            Clickable clickable = ((Clickable) inFront);
            clickable.setAllowed(true);
        }
        return false;
    }
}
