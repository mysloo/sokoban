package com.sokoban.elements.strategies;

import com.sokoban.elements.*;
import com.sokoban.game.Game;
import com.sokoban.game.GameMap;

import java.awt.*;

public class MoveElementStrategy implements ElementStrategy {

    @Override
    public boolean performAction(Game game) {
        GameMap map = game.getMap();
        Player player = game.getPlayer();
        Point playerPoint = map.getPlayerPoint();

        Point pointInFront = new Point(playerPoint.x + player.getDirection().getX(),
                playerPoint.y + player.getDirection().getY());
        Element inFront = map.getElement(pointInFront);
        if (inFront instanceof Standable) {
            if (((Standable) inFront).isStandable()) {
                Point newPlayerPoint = player.move(playerPoint);
                map.update(player, playerPoint, newPlayerPoint);
                game.setSteps(game.getSteps() + 1);
                return true;
            }
        }
        return false;
    }


}
