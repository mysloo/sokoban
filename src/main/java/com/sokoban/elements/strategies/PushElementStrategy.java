package com.sokoban.elements.strategies;

import com.sokoban.elements.*;
import com.sokoban.game.Game;
import com.sokoban.game.GameMap;

import java.awt.*;

public class PushElementStrategy implements ElementStrategy {

    @Override
    public boolean performAction(Game game) {
        GameMap map = game.getMap();
        Player player = game.getPlayer();
        Point playerPoint = map.getPlayerPoint();
        Point pointInFront = new Point(playerPoint.x + player.getDirection().getX(),
                playerPoint.y + player.getDirection().getY());
        Element inFront = map.getElement(pointInFront);
        if (inFront instanceof Pushable) {
            Point behindPointInFront = new Point(pointInFront.x + player.getDirection().getX(),
                    pointInFront.y + player.getDirection().getY());
            Element behindPushable = map.getElement(behindPointInFront);
            if (behindPushable instanceof Standable) {
                if (((Standable) behindPushable).isStandable()) {
                    Point pushedPoint = (player).push((Pushable) inFront, pointInFront);
                    map.update(inFront, pointInFront, pushedPoint);
                    player.doAction(game);
                }
                return true;
            }
        }
        return false;
    }
}
