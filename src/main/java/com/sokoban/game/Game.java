package com.sokoban.game;

import com.sokoban.components.GameWindow;
import com.sokoban.config.SokobanConfig;
import com.sokoban.elements.*;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Game extends JPanel implements Drawable {
    private static GameController gameController;
    private static final SokobanConfig config = SokobanConfig.getInstance();

    private GameMap map;
    private Player player;
    private Lever lever;
    private Obstacle obstacle;
    private Game state;
    private int steps;

    public Game(Game prevGame) {
        map = new GameMap(prevGame.map);
        steps = prevGame.steps;
        player = prevGame.player;
    }

    public Game(MapLevel level, GameWindow gameWindow) {
        try {
            map = createMap(level);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (gameController == null) {
            gameController = new GameController(player, map, gameWindow);
            setState(this);
            gameWindow.addKeyListener(player);
            gameWindow.addKeyListener(lever);
            gameWindow.clearStates();
            gameWindow.saveToSavedStates(this.saveToMemento());
            gameWindow.initGame(this);
            new Thread(gameController).start();
        } else {
            setState(this);
            gameWindow.clearStates();
            gameWindow.saveToSavedStates(this.saveToMemento());
            gameWindow.initGame(this);
        }

    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        draw(graphics);
    }

    @Override
    public void draw(Graphics graphics) {
        map.forEach((e, list) -> {
            list.forEach((point) -> {
                e.draw(graphics, point);
            });
        });
    }

    public GameMap createMap(MapLevel level) throws IOException {
        InputStream inputStream = getClass().getResourceAsStream(level.getMapPath());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        ElementBuilder builder = new ElementBuilder(level);
        createBackground(builder);
        Point offsets = computeOffsets(level);
        builder.setOffsets(offsets);
        int j = 0;
        while ((line = bufferedReader.readLine()) != null) {
            for (int i=0;i<line.length();i++) {
                switch(line.charAt(i)) {
                    case '*':
                        builder.createWall(i, j);
                        break;
                    case 'x':
                        builder.createBox(i, j);
                        break;
                    case 'P':
                        player = builder.createPlayer(i, j);
                        break;
                    case '.':
                        builder.createEndpoint(i, j);
                        break;
                    case ' ':
                        builder.createFloor(i, j);
                        break;
                    case '=':
                        builder.createUnreachedField(i, j);
                        break;
                    case '/':
                        lever = builder.createLever(i, j);
                        break;
                    case '#':
                        obstacle = builder.createObstacle(i, j);
                        break;
                }
            }
            j++;
        }
        if (lever != null) {
            lever.setObstacle(obstacle);
        }
        return builder.getMap();
    }

    private Point computeOffsets(MapLevel level) throws IOException {
        int height = config.getMapHeight();
        int width = config.getMapWidth();
        int offsetX = 0;
        int offsetY = 0;
        String line;
        InputStream inputStream = getClass().getResourceAsStream(level.getMapPath());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = bufferedReader.readLine()) != null) {
            offsetX = Math.max(offsetX, line.length());
            offsetY++;
        }
        if (offsetX < width && offsetY < height) {
            return new Point((width - offsetX)/2, (height - offsetY)/2);
        }
        return new Point(0,0);
    }

    private void createBackground(ElementBuilder builder) {
        int height = config.getMapHeight();
        int width = config.getMapWidth();
        for (int i=0;i<width;i++) {
            for (int j=0;j<height;j++) {
                builder.createUnreachedField(i, j);
            }
        }
    }

    public void setState(Game state) {
        //copy of the com.sokoban.game
        this.state = new Game(state);
    }

    public Memento saveToMemento() {
        return new Memento(this.state);
    }

    public void restoreFromMemento(Memento memento) {
        this.state = memento.getSavedState();
    }

    public Game getState() {
        return state;
    }

    public static class Memento {

        private final Game state;

        public Memento(Game stateToSave) {
            state = stateToSave;
        }

        private Game getSavedState() {
            return state;
        }
    }


    public GameMap getMap() {
        return map;
    }

    public Player getPlayer() {
        return player;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public static GameController getGameController() {
        return gameController;
    }
}
