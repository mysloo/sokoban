package com.sokoban.game;

import com.sokoban.components.dialogs.recap.RecapDialog;
import com.sokoban.components.GameWindow;
import com.sokoban.elements.*;

import java.awt.*;
import java.util.ArrayList;

public class GameController implements Runnable {
    private final GameWindow gameWindow;
    private final ArrayList<GameObserver> observers = new ArrayList<>();
    private long start;
    private long end;

    private Player player;
    private GameMap map;
    private Game oldState;

    public GameController(Player player, GameMap map, GameWindow gameWindow) {
        this.player = player;
        this.map = map;
        this.gameWindow = gameWindow;
        oldState = gameWindow.getGame();
    }

    @Override
    public void run() {
        boolean modified = false;
        start = System.currentTimeMillis();
        Game game = gameWindow.getGame();
        while (true) {
            end = System.currentTimeMillis();
            if (oldState != gameWindow.getGame()) {
                oldState = gameWindow.getGame();
                map = oldState.getMap();
                modified = true;
            }
            if (player.isMoved()) {
                if (modified) {
                    gameWindow.adjustGame();
                    modified = false;
                }
                game = gameWindow.getGame();
                game.setState(game);
            }
            if (checkCollision()) {
                Game.Memento memento = game.saveToMemento();
                gameWindow.saveToSavedStates(memento);
            }
            player.tick();
            gameWindow.repaint();
            gameWindow.requestFocusInWindow();
            notifyObservers();
            Thread.yield();
            try {
                Thread.sleep(2 * 60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (isEndGame()) {
                Score score = new Score((int)(end-start), oldState.getSteps(), map.getLevel());
                new RecapDialog(true, gameWindow, score);
            }
        }
    }
    //TODO
    //undo(zapisywanie dzwigini oraz drzwi)
    //rozmiar planszy na podstawie properties! albo scalability i 2 rozmiary male/duze mapa
    //muzyka
    //jar!
    public boolean checkCollision() {
        Point playerPoint = map.getPlayerPoint();
        Point pointInFront = new Point(playerPoint.x + player.getDirection().getX(),
                playerPoint.y + player.getDirection().getY());
        Element inFront = map.getElement(pointInFront);
        return player.doAction(oldState) || inFront.doAction(oldState);

    }

    private boolean isEndGame() {
        return map.isAllBoxesOnEndpoint();
    }

    private void notifyObservers() {
        observers.forEach(observer -> observer.update(gameWindow.getGame()));
    }

    public void addObserver(GameObserver observer) {
        observers.add(observer);
    }

    public void update(GameMap map) {
        this.map = map;
        this.oldState = null;
        this.start = System.currentTimeMillis();
    }

    public long getTimer() {
        return end - start;
    }
}
