package com.sokoban.game;

import com.sokoban.elements.*;

import java.awt.*;
import java.util.LinkedList;
import java.util.SortedMap;
import java.util.TreeMap;

public class GameMap extends TreeMap<Element, LinkedList<Point>> {
    private static int boxesAmount;
    private static MapLevel level;

    public GameMap(MapLevel level) {
        this.level = level;
        boxesAmount = 0;
    }

    public GameMap(SortedMap sortedMap) {
        super(sortedMap);
        LinkedList<Point> boxes = new LinkedList<>();
        this.forEach((el, list) -> {
            if (el instanceof Box) {
                list.forEach((point)-> {
                    boxes.add(new Point(point));
                });
            }
        });
        Player player = (Player) ElementFactory.getElementType(Player.class);
        Box box = (Box) ElementFactory.getElementType(Box.class);
        Point p = new Point(this.get(player).get(0));
        this.remove(player);
        this.put(player, new LinkedList<>());
        this.get(player).add(p);
        this.put(box, boxes);
    }

    public void update(Element element, Point oldPoint, Point newPoint) {
        remove(element, oldPoint);
        add(element, newPoint);
    }

    private void add(Element element, Point point) {
        this.get(element).add(point);
    }

    private void remove(Element element, Point point) {
        this.get(element).remove(point);
    }

    //O(boxesAmount^2)
    public boolean isAllBoxesOnEndpoint() {
        LinkedList<Point> boxList = this.get(ElementFactory.getElementType(Box.class));
        LinkedList<Point> endpointList = this.get(ElementFactory.getElementType(Endpoint.class));
        int counter = 0;
        for (Point p1 : boxList) {
            for (Point p2 : endpointList) {
                if (p1.equals(p2)) {
                    counter++;
                    break;
                }
            }
        }
        return boxesAmount == counter;
    }

    public Point getPlayerPoint() {
        Player player = (Player) ElementFactory.getElementType(Player.class);
        return this.get(player).get(0);
    }

    public Element getElement(Point point) {
        Element[] result = new Element[1];
        this.forEach((el, list) -> {
            list.forEach((p) -> {
                if (p.equals(point)) {
                    result[0] = el;
                }
            });
        });
        return result[0];
    }

    public void incrementBoxesAmount() {
        boxesAmount++;
    }

    public MapLevel getLevel() {
        return level;
    }
}
