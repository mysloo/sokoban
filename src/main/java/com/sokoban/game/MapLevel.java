package com.sokoban.game;

import com.sokoban.config.SokobanConfig;

public enum MapLevel {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9);

    private SokobanConfig config = SokobanConfig.getInstance();
    private int index;

    MapLevel(int idx) {
        index = idx;
    }

    public String getMapPath() {
        return config.getMapsPath() + "map" + index + config.getProperExtension();
    }

    public int getIndex() {
        return index;
    }

    public MapLevel getPreviousLevel(MapLevel actual) {
        MapLevel previous = values()[0];
        for (MapLevel level : values()) {
            if (level.index + 1 == actual.index) {
                previous = level;
                break;
            }
        }
        return previous;
    }

    public MapLevel setUpNextLevel(MapLevel actual) {
        MapLevel next = values()[values().length-1];
        for (MapLevel level : values()) {
            if (level.index - 1 == actual.index) {
                next = level;
                break;
            }
        }
        return next;
    }
}
