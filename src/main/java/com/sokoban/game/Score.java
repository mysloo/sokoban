package com.sokoban.game;

import java.io.Serializable;

public class Score implements Comparable<Score>, Serializable {
    private int time;
    private int steps;
    private MapLevel level;

    public Score(int time, int steps, MapLevel level) {
        this.time = time;
        this.steps = steps;
        this.level = level;
    }

    public int getTime() {
        return time;
    }

    public int getSteps() {
        return steps;
    }

    public MapLevel getLevel() {
        return level;
    }

    @Override
    public int compareTo(Score that) {
        return ((this.steps - that.steps) + (this.time - that.time));
    }

}
