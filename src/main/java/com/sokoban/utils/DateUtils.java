package com.sokoban.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static String numberToStringDate(long number, String format) {
        Date date = new Date(number);
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
}
