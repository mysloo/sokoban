package com.sokoban.utils;

import com.sokoban.config.SokobanConfig;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class ImageUtils {

    public static BufferedImage createImage(String file) {
        if (file == null) {
            return null;
        }
        InputStream inputStream = ImageUtils.class.getResourceAsStream(SokobanConfig.getInstance().getImagesPath() + file);
        BufferedImage img = null;
        try {
            img = ImageIO.read(inputStream);
        } catch (IOException e) {
            System.out.println(file);
            e.printStackTrace();
        }
        return img;
    }

}
