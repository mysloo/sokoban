package com.sokoban.utils;

import java.io.*;

public class SerializationUtils {
    private static final String PROP_STATS_PATH = "src/main/resources/stats/";

    public static void serialize(Object obj, String fileName) throws IOException {
        FileOutputStream fos = new FileOutputStream(PROP_STATS_PATH + fileName);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(obj);
        oos.close();
    }

    public static Object deserialize(String fileName) throws IOException, ClassNotFoundException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(PROP_STATS_PATH + fileName);
        } catch (FileNotFoundException e) {
            File file = new File(PROP_STATS_PATH + fileName);
            file.createNewFile();
            fis = new FileInputStream(PROP_STATS_PATH + fileName);
        }
        BufferedInputStream bis = new BufferedInputStream(fis);
        ObjectInputStream ois = new ObjectInputStream(bis);
        Object obj = ois.readObject();
        ois.close();
        return obj;

    }
}